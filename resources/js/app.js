require('./bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import Bootstrap from 'bootstrap-vue';
import Vuex from 'vuex';
import Toasted from 'vue-toasted';

Vue.use(VueRouter);
Vue.use(Bootstrap);
Vue.use(Vuex);
Vue.use(Toasted, {
    position: "top-center",
    duration: 10000,
    type: "info"
  });


import ClientBaseComponent from './components/clientBaseComponent'
import BackofficeBaseComponent from './backoffice/backofficeBaseComponent'
import WelcomeComponent from './components/welcome'
import AboutComponent from './components/about'
import MenusComponent from './components/menus'
import ErrorsComponent from './components/errors'
import RestaurantsComponent from './components/restaurants'
import AppPromotionComponent from './components/appPromotion'
import LoginComponent from'./auth/login'
import DashboardComponent from'./backoffice/dashboard'
import NavbarTopComponent from'./navbar/navbar_top'
import NavbarSideComponent from'./navbar/navbar_side'
import FooterBotComponent from'./footer/footer_bot'
import UsersComponent from'./users/list'
import UserCreate from './users/create'
import UserInformation from './users/edit'
import BackofficeRestaurantComponent from'./restaurants/list'
import RestaurantCreate from'./restaurants/create'
import RestaurantEdit from'./restaurants/edit'
import BackofficeItemsComponent from'./items/list'
import ItemsCreate from'./items/create'
import ItemEdit from'./items/edit'
import BackofficeCategoriesComponent from'./categories/list'
import CategoryCreate from'./categories/create'

import BackofficeSchedulesComponent from'./schedules/list'
import ScheduleCreate from'./schedules/create'
import ScheduleEdit from'./schedules/edit'
import ScheduleExceptionCreate from'./schedules/exception/create'
import ScheduleExceptionEdit from'./schedules/exception/edit'

import BackofficeAllergensComponent from'./allergens/list'
import AllergenCreate from'./allergens/create'
import AllergenEdit from'./allergens/edit'
import BackofficePromotionsComponent from'./promotions/list'
import PromotionCreate from'./promotions/create'
import PromotionEdit from'./promotions/edit'
import BackofficeMessagesComponent from'./messages/list'
import MessageCreate from'./messages/create'
import BackofficeTakeawaysComponent from'./takeaways/list'
import TakeawayEdit from'./takeaways/edit'
import BackofficeTakeawayComponent from'./takeaways/order'
import BackofficeReservationsComponent from'./reservations/list'
import BackofficeReservationsCreateComponent from'./reservations/create'
import ReservationEdit from'./reservations/edit'
import BackofficeReservationComponent from'./reservations/order'
import RestaurantDetailsComponent from'./restaurants/details'
import RestaurantMenuComponent from'./restaurants/menu'


Vue.component('user-information', UserInformation);
Vue.component('user-create', UserCreate);
Vue.component('restaurant-create', RestaurantCreate);
Vue.component('restaurant-edit', RestaurantEdit);
Vue.component('takeaway-edit', TakeawayEdit);
Vue.component('reservation-edit', ReservationEdit);
Vue.component('category-create', CategoryCreate);
Vue.component('schedule-create', ScheduleCreate);
Vue.component('schedule-edit', ScheduleEdit);
Vue.component('schedule-exception-create', ScheduleExceptionCreate);
Vue.component('schedule-exception-edit', ScheduleExceptionEdit);
Vue.component('allergen-create', AllergenCreate);
Vue.component('allergen-edit', AllergenEdit);
Vue.component('promotion-create', PromotionCreate);
Vue.component('promotion-edit', PromotionEdit);
Vue.component('message-create', MessageCreate);
Vue.component('restaurant-category-item-create', ItemsCreate);
Vue.component('item-edit', ItemEdit);
Vue.component('navbar-top', NavbarTopComponent);
Vue.component('navbar-side', NavbarSideComponent);
Vue.component('footer-bot', FooterBotComponent);
Vue.component('validation-errors', ErrorsComponent);
const welcome = Vue.component('welcome', WelcomeComponent);
const about = Vue.component('about', AboutComponent);
const menus = Vue.component('menus', MenusComponent);
const restaurants = Vue.component('restaurants', RestaurantsComponent);
const restaurantDetails = Vue.component('restaurantDetails', RestaurantDetailsComponent);
const restaurantMenu = Vue.component('restaurantMenu', RestaurantMenuComponent);
const appPromotion = Vue.component('appPromotion', AppPromotionComponent);
const login = Vue.component('login', LoginComponent);
const dashboard = Vue.component('backoffice', DashboardComponent);
const users = Vue.component('users', UsersComponent);
const backofficeRestaurants = Vue.component('backofficeRestaurants', BackofficeRestaurantComponent);
const backofficeCategories = Vue.component('backofficeCategories', BackofficeCategoriesComponent);
const backofficeItems = Vue.component('backofficeItems', BackofficeItemsComponent);
const backofficeSchedules = Vue.component('backofficeSchedules', BackofficeSchedulesComponent);
const backofficeAllergens = Vue.component('backofficeAllergens', BackofficeAllergensComponent);
const backofficePromotions = Vue.component('backofficePromotions', BackofficePromotionsComponent);
const backofficeMessages = Vue.component('backofficeMessages', BackofficeMessagesComponent);
const backofficeTakeaways = Vue.component('backofficeTakeaways', BackofficeTakeawaysComponent);
const backofficeReservations = Vue.component('backofficeReservations', BackofficeReservationsComponent);
const backofficeReservationsCreate = Vue.component('backofficeReservationsCreate', BackofficeReservationsCreateComponent);
const backofficeTakeaway = Vue.component('backofficeTakeaway', BackofficeTakeawayComponent);
const backofficeReservation = Vue.component('backofficeReservation', BackofficeReservationComponent);


const routes = [
    {path:'/', component: ClientBaseComponent, children: [
        {path:'', name: 'welcome', component: welcome},
        {path:'/about', component: about},
        {path:'/restaurants', component: restaurants},
        {path:'/menus', component: menus},
        {path:'/restaurant/:restaurantName/details', component: restaurantDetails},
        {path:'/restaurant/:restaurantName/menu', component: restaurantMenu},
        {path:'/appPromotion', component: appPromotion},
    ]},
    {path:'/backoffice', component: BackofficeBaseComponent, children: [
      {path:'/backoffice/login', component: login, meta: { notAuthenticated: true }},
      {path:'/backoffice/dashboard', name:'dashboard', component: dashboard , meta: { forAuth: true, forAdminOrEmployee: true }},
      {path:'/backoffice/users', component: users , meta: { forAuth: true, forAdmin: true }},
      {path:'/backoffice/restaurants', component: backofficeRestaurants , meta: { forAuth: true, forAdmin: true }},
      {path:'/backoffice/restaurant/:restaurantName/categories', component: backofficeCategories , meta: { forAuth: true, forAdmin: true }},
      {path:'/backoffice/restaurant/:restaurantName/schedules', component: backofficeSchedules , meta: { forAuth: true, forAdmin: true }},
      {path:'/backoffice/restaurant/:restaurantName/:categoryName/items', component: backofficeItems , meta: { forAuth: true, forAdmin: true }},
      {path:'/backoffice/restaurant/:restaurantName/:categoryName/:itemName/allergens', component: backofficeAllergens , meta: { forAuth: true, forAdmin: true }},
      {path:'/backoffice/promotions', component: backofficePromotions , meta: { forAuth: true, forAdmin: true }},
      {path:'/backoffice/messages', component: backofficeMessages , meta: { forAuth: true, forAdmin: true }},
      {path:'/backoffice/takeaways', component: backofficeTakeaways , meta: { forAuth: true, forAdminOrEmployee: true }},
      {path:'/backoffice/reservations', component: backofficeReservations , meta: { forAuth: true, forAdminOrEmployee: true }},
      {path:'/backoffice/reservations/create', component: backofficeReservationsCreate , meta: { forAuth: true, forAdminOrEmployee: true }},
      {path:'/backoffice/takeaway/:takeawayId/order', component: backofficeTakeaway , meta: { forAuth: true, forAdminOrEmployee: true }},
      {path:'/backoffice/reservation/:reservationId/order', component: backofficeReservation , meta: { forAuth: true, forAdminOrEmployee: true }},
    ]},
]


const router = new VueRouter({
    routes: routes,
    scrollBehavior (to, from, savedPosition) {
      if (to.hash) {
        return {
          selector: to.hash
        };
      }
    }
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.notAuthenticated)) {
    if (sessionStorage.getItem("id_token") != null) {
      next("/");
      return;
    } else next();
  }
  else if (to.matched.some(record => record.meta.forAuth)) {
    if (sessionStorage.getItem("id_token") == null) { 
      next("/backoffice/login");
      return;
    } else next()
  }
  else if (to.matched.some(record => record.meta.forAdminOrEmployee)) {
    if (this.$store.getters.userRole != 'admin' && this.$store.getters.userRole != 'employee') {
      next("/");
      return;
    } else next()
  }
  else if (to.matched.some(record => record.meta.forAdmin)) {
    if (this.$store.getters.userRole != 'admin') {
      next("/");
      return;
    } else next()
  }
  next();
});


const store = new Vuex.Store({
    state: {
        id_token: null,
        refresh_token: null,
        uid: "",
        role: "",
    },
    getters: {
        user(state) {
          return Object.assign({}, state.user)
        },
        isUserAuth(state) {
          return state.access_token != null;
        }
      },
    mutations: {
        setIdToken(state, token) {
          state.id_token = token;
        },
        setRefreshToken(state, token) {
          state.refresh_token = token;
        },
        setUid(state, uid) {
          state.uid = uid;
        },
        setUserRole(state, role) {
          state.role = role;
        }
      },
      actions: {
        setIdToken(state, token) {
          state.commit('setIdToken', token);
        },
        setRefreshToken(state, token) {
          state.commit('setRefreshToken', token);
        },
        setUid(state, uid) {
          state.commit('setUid', uid);
        },
        setUserRole(state, role) {
          state.commit('setUserRole', role);
        },
      }
});


axios.interceptors.response.use((response) => {
    return response;
  }, (error) => {
    if(error.response.status === 401 && error.response.data === "Expired Token"){ //refresh token
        app.loginRefreshToken();
        return Promise.reject(error);
    }
    app.genericErrorMessage(error);
    return Promise.reject(error);
});


const app = new Vue({
    el: '#app',
    router,
    store,
    data: {
      
    },
    methods: {
      genericErrorMessage(error){
        console.log('Status: ' + error.response.status + " | Message: " + error.response.data)
        this.$toasted.show('Ups! Aconteceu alguma coisa de errado');
      },
      invalidToken(){
        this.removeAuthParams();
        this.$router.push('/backoffice/login').catch(error =>{
          console.log(error);
        });
      },
      logout(){
        axios.post("api/logout", {}, { 
          headers: { Authorization: "Bearer " + this.$store.state.id_token }
        })
          .then(() => {
            this.removeAuthParams();

            if(this.$router.currentRoute.name != "welcome"){
              this.$router.push('/').catch(error =>{
                console.log(error);
              });
            }
          })
          .catch((error) => {
              this.$toasted.show('Erro: Não foi possivel fazer logout tente outra vez');
          });
      },
      loginRefreshToken:  function(){
        this.$toasted.show('Sessão expirou.');
        app.removeAuthParams();

        router.push('/backoffice/login').catch(err =>{
          console.log(err);
        });

        /* falta fazer o pedido que foi denied automaticamente
        return axios.put("api/signInWithRefreshToken", {
            refreshToken: sessionStorage.getItem('refresh_token')
          }).then((responseLogin) => {
            app.$store.dispatch("setIdToken", responseLogin.data.token.id_token);
            app.$store.dispatch("setRefreshToken", responseLogin.data.token.refresh_token);
            app.$store.dispatch("setUid", responseLogin.data.user.uid);
            app.$store.dispatch("setUserRole", responseLogin.data.user.role);
            sessionStorage.setItem("id_token", responseLogin.data.token.id_token);
            sessionStorage.setItem("refresh_token", responseLogin.data.token.refresh_token);
            sessionStorage.setItem("uid", responseLogin.data.user.uid);
            sessionStorage.setItem("role", responseLogin.data.user.role);
            
            this.$toasted.show('Sessão expirou, login efetuado novamente automaticamente.');
            router.push('/backoffice/dashboard').catch(err =>{
              console.log(err);
            });
          })
          .catch((e) => {
            app.removeAuthParams();

            router.push('/backoffice/login').catch(err =>{
              console.log(err);
            });
          })
          */
      },
      removeAuthParams(){
        this.$store.dispatch("setIdToken", null);
        this.$store.dispatch("setRefreshToken", null);
        this.$store.dispatch("setUid", null);
        this.$store.dispatch("setUserRole", null);
        sessionStorage.removeItem('id_token');
        sessionStorage.removeItem('refresh_token');
        sessionStorage.removeItem('uid');
        sessionStorage.removeItem('role');
      }
    },
    mounted() {
        /* quando se faz refresh a pagina continua-se logged in */
        this.$store.dispatch("setIdToken", sessionStorage.getItem('id_token'));
        this.$store.dispatch("setRefreshToken", sessionStorage.getItem('refreshToken'));
        this.$store.dispatch("setUserRole", sessionStorage.getItem('role'));
        this.$store.dispatch("setUid", sessionStorage.getItem('uid'));
      }
});
