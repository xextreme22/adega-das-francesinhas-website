@extends('master')

@section('title', 'A Adega Das Francesinhas')

@section('extrastyles')
  <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> <!-- used for the icons -->
@endsection

@section('content')

<router-view></router-view>

@endsection

@section('pagescript')
<script src="js/app.js"></script>
@stop