<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ScheduleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'horaAbertura' => 'required|date_format:H:i',
            'horaFecho' => 'required|date_format:H:i',
            'diaSemana' => ['required', Rule::in(['segunda', 'terca', 'quarta', 'quinta', 'sexta', 'sabado', 'domingo'])],
            'aceitaReservas' => ['required', Rule::in([true, false])],
            'aceitaTakeaways' => ['required', Rule::in([true, false])],
        ];
    }
}
