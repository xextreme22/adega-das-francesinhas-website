<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ScheduleExceptionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'aceitaReservas' => [Rule::requiredIf(is_null($this->aceitaTakeaways)), Rule::in([true, false])],
            'aceitaTakeaways' => [Rule::requiredIf(is_null($this->aceitaReservas)), Rule::in([true, false])],
        ];
    }
}
