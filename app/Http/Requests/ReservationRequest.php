<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data' => 'required|date|after:now',
            'telemovel' => 'required|integer|gte:100',
            'nome' => 'required|min:2|max:64',
            'comentario' => 'min:0|max:255',
            'numeroPessoas' => 'required|integer|gte:1',
            'nomeRestaurante' => 'required|min:2|max:64',
        ];
    }
}
