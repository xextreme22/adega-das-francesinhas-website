<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pontos' => 'required|integer|gte:0',
            'role' => ['required', Rule::in(['admin', 'employee'])],
            'nome' => 'required|min:2|max:64',
            'password' => 'nullable|min:3|max:20',
            'telemovel' => 'required|min:1|regex:/^\+[1-9]\d{1,14}$/'
        ];
    }
}
