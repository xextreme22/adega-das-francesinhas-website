<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PromotionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nomeRestaurante' => 'required|min:2|max:64',
            'nomeCategoria' => 'required|min:2|max:64',
            'nomeItem' => 'required|min:2|max:64',
            'pontos' => 'required|integer|gte:0',
        ];
    }
}
