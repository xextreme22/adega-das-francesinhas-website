<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RestaurantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|min:2|max:64',
            'telefone' => 'required|integer|gte:100',
            'horario' => 'required|min:2|max:255',
            'lat' => 'required|between:-90,90|numeric',
            'lon' => 'required|between:-180,180|numeric',
            'numeroMaxPorReserva' => 'required|integer|gte:1'
        ];
    }
}
