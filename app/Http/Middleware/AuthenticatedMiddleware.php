<?php

namespace App\Http\Middleware;

use Closure;
use Kreait\Firebase\Factory;
use Google\Auth\FetchAuthTokenInterface;
use Google\Auth\FetchAuthTokenCache;
use Kreait\Firebase\Exception\InvalidArgumentException;
use Firebase\Auth\Token\Exception\InvalidToken;
use Firebase\Auth\Token\Exception\ExpiredToken;

class AuthenticatedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->bearerToken();
        if($token != "null"){
            $firebase = (new Factory())->withServiceAccount(base_path().'\\FirebaseKey.json');
            $auth = $firebase->createAuth();

            try{
                $verifiedIdToken = $auth->verifyIdToken($token);
                return $next($request);
            } catch (ExpiredToken $e) {
                return response('Expired Token', 401);
            } catch (InvalidToken $e) {
                return response('Invalid Token', 401);
            } catch (InvalidArgumentException $e) {
                return response('Unauthorized', 401);
            }
        }
        return response('Unauthorized', 401);
    }

}
