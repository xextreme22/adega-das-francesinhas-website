<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use App\Http\Requests\RestaurantRequest;
use App\Http\Requests\RestaurantUpdateRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use DateTime;
use DateTimeZone;

class RestaurantController extends Controller
{
    private $firebase;
    private $firestore;
    private $storageBucket;

    public function __construct(){
        $this->firebase = (new Factory())->withServiceAccount(base_path().'\\FirebaseKey.json');
        $this->firestore = $this->firebase->createFirestore()->database();
        $this->storageBucket = $this->firebase->createStorage()->getBucket();
    }

    private function createDirectoryIfDoesntExist(){
        $path = 'storage\\fotos\\restaurantes';
        $pathDirectory = public_path().'\\' . $path;
        File::makeDirectory($path, 0777, true, true);
    }

    public function index(Request $request)
    {
        $this->createDirectoryIfDoesntExist();

        $documents = $this->firestore->collection('Restaurantes')->documents();

        $doc=null;
        $i=0;
        foreach ($documents as $document) {
            if($document->exists()){
                $doc[$i] = $document->data();
                $doc[$i]['nome'] = $document->id();
                $image = 'storage\\fotos\\restaurantes\\'.$document->id().'.jpg';
                $doc[$i]['photo'] = '\\'.$image;

                $object = $this->storageBucket->object('restaurantes/'.$document->id().'.jpg');
                if (!file_exists( public_path() . '\\'.$image)) {
                    if($object->exists()){
                        $object->downloadToFile($image);
                    }else{
                        $doc[$i]['photo'] = '\\static\\placeholder.jpg';
                    }
                }
                $i++;
            }
        }

        return response()->json(['restaurantes'=>$doc], 200);
    }

    public function details(Request $request, $restaurantId)
    {
        $snapshot = $this->firestore->collection('Restaurantes')->document($restaurantId)->snapshot();
        
        if ($snapshot->exists() && !($snapshot->data()['disabled'])) {
            $data = [];
            $data['lat'] = $snapshot->data()['lat'];
            $data['nome'] = $restaurantId;
            $data['lon'] = $snapshot->data()['lon'];
            $data['horario'] = $snapshot->data()['horario'];
            $data['telefone'] = $snapshot->data()['telefone'];

            $this->createDirectoryIfDoesntExist();
            $image = 'storage\\fotos\\restaurantes\\'.$restaurantId.'.jpg';
            $data['image'] = '\\'.$image;

            if (!file_exists( public_path() . '\\'.$image)) {
                $object = $this->storageBucket->object('restaurantes/'.$restaurantId.'.jpg');
                if($object->exists()){
                    $object->downloadToFile($image);
                }else{
                    $data['image'] = '\\static\\placeholder.jpg';
                }
            }

            return response()->json(['restaurant'=>$data], 200);
        }else{
            return response()->json(['errors'=>"Invalid restaurant"], 422);
        }
    }

    public function menu(Request $request, $restaurantId)
    {
        $documentsCategories = $this->firestore->collection('Restaurantes')->document($restaurantId)->collection('Categorias')->documents();
        
        $pathStart = '/storage/fotos/restaurantes/menu/';
        $pathEnding = '.jpg';
        $menu=null;
        $this->createDirectoryIfDoesntExist();
        foreach ($documentsCategories as $category) {
            if($category->exists() && !($category->data()['disabled'])){
                $categoryName = $category->id();

                $j=0;
                $documentsItems = $this->firestore->collection('Restaurantes')->document($restaurantId)
                                                ->collection('Categorias')->document($categoryName)
                                                ->collection('Items')->documents();
                foreach ($documentsItems as $item) {
                    if($item->exists() && !($item->data()['disabled'])){
                        $menu[$categoryName][$j]['nome'] = $item->id();
                        $menu[$categoryName][$j]['ingredientes'] = $item->data()['ingredientes'];
                        $menu[$categoryName][$j]['preco'] = $item->data()['preco'];

                        $image = $pathStart.$item->id().$pathEnding;
                        if (!file_exists( public_path() . '\\'.$image)) {
                            $object = $this->storageBucket->object('restaurantes/menu/'.$item->id().$pathEnding);
                            if($object->exists()){
                                $object->downloadToFile($image);
                            }else{
                                $menu[$categoryName][$j]['photo'] = '\\static\\placeholder.jpg';
                            }
                        }
                        $j++;
                    }
                }

            }
        }

        $image = 'storage\\fotos\\restaurantes\\'.$restaurantId.'.jpg';
        $restaurant['image'] = '\\'.$image;
        if (!file_exists( public_path() . '\\'.$image)) {
            $object = $this->storageBucket->object('restaurantes/'.$restaurantId.'.jpg');
            if($object->exists()){
                $object->downloadToFile($image);
            }else{
                $data['image'] = '\\static\\placeholder.jpg';
            }
        }

        return response()->json(['menu'=>$menu, 'pathStart'=>$pathStart, 'pathEnding'=>$pathEnding, 'restaurant'=>$restaurant], 200);
    }

    public function names(Request $request)
    {
        $documents = $this->firestore->collection('Restaurantes')->where('disabled', '=', false)->documents();

        $doc=null;
        $i=0;
        foreach ($documents as $document) {
            if($document->exists()){
                $doc[$i]['nome'] = $document->id();
                $i++;
            }
        }

        return response()->json(['names'=>$doc], 200);
    }

    public function namesAndImages(Request $request)
    {
        $documents = $this->firestore->collection('Restaurantes')->where('disabled', '=', false)->documents();

        $doc=null;
        $i=0;
        $this->createDirectoryIfDoesntExist();
        foreach ($documents as $document) {
            if($document->exists()){
                $doc[$i]['nome'] = $document->id();
                $image = 'storage\\fotos\\restaurantes\\'.$document->id().'.jpg';
                $doc[$i]['image'] = '\\'.$image;

                $object = $this->storageBucket->object('restaurantes/'.$document->id().'.jpg');
                if (!file_exists( public_path() . '\\'.$image)) {
                    if($object->exists()){
                        $object->downloadToFile($image);
                    }else{
                        $doc[$i]['image'] = '\\static\\placeholder.jpg';
                    }
                }
                $i++;
            }
        }

        return response()->json(['restaurants'=>$doc], 200);
    }

    public function store(RestaurantRequest $request)
    {
        try{
            /*
            $nome = strtolower($request->nome);
            */
            $nome = $request->nome;
            if($request->photo !== null){
                if (strpos($request->input('photo'), 'data:image/') !== false) {
                    $exploded = explode(',', $request->photo);
                    $image = base64_decode($exploded[1]);
                    if (!Str::contains($exploded[0], 'jpeg') && !Str::contains($exploded[0], 'jpg')) {
                        return response()->json(['errors'=>array("photo" => "Error incorrect Photo extension (only accepts jpg or jpeg)")], 422);
                    }
                    if(strlen($image) > 200000){
                        return response()->json(['errors'=>array("photo" => "Image size cannot exceed 200KB for perfomance reasons on mobile phones")], 422);
                    }
                    
                    $name_image='restaurantes/'.$nome.".jpg";
                    $this->storageBucket->upload(
                        $image, 
                        ['name' => $name_image]
                    );
                }else{
                    return response()->json(['errors'=>array("photo" => "Photo does not corresponde to an image file")], 422);
                }
            }else{
                return response()->json(['errors'=>array("photo" => "Imagem é obrigatória")], 422);
            }

            $data = [
                'horario' => $request->horario,
                'telefone' => intval($request->telefone),
                'disabled' => false,
                'lon' => floatval($request->lon),
                'lat' => floatval($request->lat),
                'numeroMaxPorReserva' => intval($request->numeroMaxPorReserva)
            ];

            $storedData = $this->firestore->collection('Restaurantes')->document($nome)->set($data);
            $data['nome'] = $nome;
            
            $object = $this->storageBucket->object('restaurantes/'.$nome.'.jpg');
            $path = 'storage/fotos/restaurantes/'.$nome.'.jpg';
            $data['photo'] = $path;
            $this->createDirectoryIfDoesntExist();
            $object->downloadToFile($path);
            return response()->json(['data'=>$data], 201);
        } catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function update(RestaurantUpdateRequest $request, $restaurantId)
    {
        /*
        $restaurantId = strtolower($restaurantId);
        */
        if($request->photo !== null){
            if (strpos($request->input('photo'), 'data:image/') !== false) {
                $exploded = explode(',', $request->photo);
                $image = base64_decode($exploded[1]);
                if (!Str::contains($exploded[0], 'jpeg') && !Str::contains($exploded[0], 'jpg')) {
                    return response()->json(['errors'=>array("photo" => "Error incorrect Photo extension (only accepts jpg or jpeg)")], 422);
                }
                if(strlen($image) > 200000){
                    return response()->json(['errors'=>array("photo" => "Image size cannot exceed 200KB for perfomance reasons on mobile phones")], 422);
                }
                
                $name_image='restaurantes/'.$restaurantId.".jpg";
                $this->storageBucket->upload(
                    $image, 
                    ['name' => $name_image]
                );

                $object = $this->storageBucket->object('restaurantes/'.$restaurantId.'.jpg');
                $path = 'storage/fotos/restaurantes/'.$restaurantId.'.jpg';
                $this->createDirectoryIfDoesntExist();
                $object->downloadToFile($path);
            }else{
                return response()->json(['errors'=>array("photo" => "Photo does not corresponde to an image file")], 422);
            }
        }
        
        $propertiesRestaurant = [
            ['path' => 'lat', 'value' => floatval($request->lat)],
            ['path' => 'lon', 'value' => floatval($request->lon)],
            ['path' => 'telefone', 'value' => intval($request->telefone)],
            ['path' => 'horario', 'value' => $request->horario],
            ['path' => 'numeroMaxPorReserva', 'value' => intval($request->numeroMaxPorReserva)],
        ];

        $this->firestore->collection('Restaurantes')->document($restaurantId)->update($propertiesRestaurant);

        $restauranteUpdated = [
            'lat' => $request->lat,
            'lon' => $request->lon,
            'telefone' => $request->telefone,
            'horario' => $request->horario,
            'photo' => 'storage/fotos/restaurantes/'.$restaurantId.'.jpg',
            'numeroMaxPorReserva' => intval($request->numeroMaxPorReserva),
        ];

        return response()->json(['restaurant'=>$restauranteUpdated], 200);
    }

    public function activate($restaurantId)
    {
        try{
            /*
            $restaurantId = strtolower($restaurantId);
            */
            $this->firestore->collection('Restaurantes')->document($restaurantId)->update([
                ['path' => 'disabled', 'value' => false]
            ]);
            return response()->json(['msg'=>'Restaurante ativado'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function deactivate($restaurantId)
    {
        try{
            /*
            $restaurantId = strtolower($restaurantId);
            */
            $this->firestore->collection('Restaurantes')->document($restaurantId)->update([
                ['path' => 'disabled', 'value' => true]
            ]);
            return response()->json(['msg'=>'Restaurante desativado'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function schedules(Request $request, $restaurantId)
    {
        $documents = $this->firestore->collection('Restaurantes')->document($restaurantId)->collection('Horarios')->documents();

        $schedules=null;
        $i=0;
        foreach ($documents as $document) {
            if($document->exists()){
                $schedules[$i] = $document->data();
                $schedules[$i]['id'] = $document->id();
                $schedules[$i]['horaAbertura'] = DateTime::createFromFormat('Hi', $document->data()['horaAbertura'])->format('H:i');
                $schedules[$i]['horaFecho'] = DateTime::createFromFormat('Hi', $document->data()['horaFecho'])->format('H:i');
                $i++;
            }
        }

        $documents = $this->firestore->collection('Restaurantes')->document($restaurantId)->collection('ExcecoesHorario')->documents();

        $scheduleExceptions=null;
        $i=0;
        foreach ($documents as $document) {
            if($document->exists()){
                $scheduleExceptions[$i] = $document->data();
                $scheduleExceptions[$i]['id'] = $document->id();
                $scheduleExceptions[$i]['dataInicio'] = $document->data()['dataInicio']->get()->setTimezone(new DateTimeZone('Europe/Lisbon'))->format('Y-m-d H:i');
                $scheduleExceptions[$i]['dataFim'] = $document->data()['dataFim']->get()->setTimezone(new DateTimeZone('Europe/Lisbon'))->format('Y-m-d H:i');
                $i++;
            }
        }

        return response()->json(['schedules'=>$schedules, 'scheduleExceptions' => $scheduleExceptions], 200);
    }

    public function categories(Request $request, $restaurantId)
    {
        /*
        $restaurantId = strtolower($restaurantId);
        */
        $documents = $this->firestore->collection('Restaurantes')->document($restaurantId)->collection('Categorias')->documents();

        $doc=null;
        $i=0;
        foreach ($documents as $document) {
            if($document->exists()){
                $doc[$i] = $document->data();
                $doc[$i]['nome'] = $document->id();
                $i++;
            }
        }

        return response()->json(['categorias'=>$doc], 200);
    }

}
