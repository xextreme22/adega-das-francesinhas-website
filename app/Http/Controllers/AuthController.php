<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use InvalidArgumentException;
use Kreait\Firebase\Exception\Auth\RevokedIdToken;
use Kreait\Firebase\Auth\SignIn\FailedToSignIn;
use Firebase\Auth\Token\Exception\ExpiredToken;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\ResetPasswordRequest;
use RuntimeException;

class AuthController extends Controller
{
    private $firebase;
    private $auth;
    private $firestore;

    public function __construct(){
        //$this->firebase = (new Factory())->withServiceAccount(base_path(env('GOOGLE_APPLICATION_CREDENTIALS')));
        $this->firebase = (new Factory())->withServiceAccount(base_path().'\\FirebaseKey.json');
        $this->auth = $this->firebase->createAuth();
        $this->firestore = $this->firebase->createFirestore()->database();
    }

    public function signInWithEmail(LoginRequest $request){
        try{
            $signInResult = $this->auth->signInWithEmailAndPassword($request->email, $request->password);

            $user = $this->auth->getUser($signInResult->data()["localId"]);

            if($user->emailVerified){
                $userParams['uid'] = $user->uid;
                $userParams['role'] = $user->customAttributes['role'];

                return response()->json([
                    'msg'=>'Logged In', 
                    'token'=>$signInResult->asTokenResponse(),
                    'user'=>$userParams
                ], 200);
            }
        }catch (FailedToSignIn $e){
            return response()->json(['errors'=>['Email não existe ou a password introduzida não está correta, certifique-se que tem o email verificado tambem.']], 422);
        }
        return response()->json(['errors'=>['Email não existe ou a password introduzida não está correta, certifique-se que tem o email verificado tambem.']], 422);
    }

    public function resetPassword(ResetPasswordRequest $request){
        try{
            $this->auth->sendPasswordResetLink($request->email);
            return response()->json(['msg'=>'Foi enviado um email para redefenir a senha.'], 200);
        }catch (InvalidArgumentException $e){
            return response()->json(['msg'=>'Foi enviado um email para redefenir a senha.'], 200);
        }catch (RuntimeException $e){
            return response()->json(['msg'=>'Foi enviado um email para redefenir a senha.'], 200);
        }
    }

    public function signInWithRefreshToken(Request $request){
        try{
            $signInResult = $this->auth->signInWithRefreshToken($request->refreshToken);

            $user = $this->auth->getUser($signInResult->data()["user_id"]);

            if($user->emailVerified){
                $userParams['uid'] = $user->uid;
                $userParams['role'] = $user->customAttributes['role'];

                return response()->json([
                    'msg'=>'Logged In', 
                    'token'=>$signInResult->asTokenResponse(),
                    'user'=>$userParams
                ], 200);
            }
        } catch (FailedToSignIn $e){
            dd("asd");
            return response()->json(['errors'=>['Email não existe ou a password introduzida não está correta, certifique-se que tem o email verificado tambem.']], 422);
        }
        dd("asd");
        return response()->json(['errors'=>['Email não existe ou a password introduzida não está correta, certifique-se que tem o email verificado tambem.']], 422);
    }

    /*
    public function verifyIdToken(Request $request){
        try{
            $verifiedIdToken = $this->auth->verifyIdToken($request->bearerToken());

            return response()->json(['token'=>$verifiedIdToken], 200);
        } catch (InvalidToken $e) {
            return response()->json(['errors'=>['invalid token']], 401); 
        } catch (ExpiredToken $e) {
            return response()->json('Token Expired', 401);
        }
    }
    */

    public function logout(Request $request)
    {
        try{
            $verifiedIdToken = $this->auth->verifyIdToken($request->bearerToken());
            $UID = $verifiedIdToken->getClaim('sub');
            $this->auth->revokeRefreshTokens($UID);
            
            try {
                $verifiedIdToken = $this->auth->verifyIdToken($request->bearerToken(), $checkIfRevoked = true);
            } catch (RevokedIdToken $e) {
                return response()->json(['msg'=>'Token Revoked'], 200);
            }
        } catch (InvalidToken $e) {
            return response('Invalid Token', 400);
        }
    }
}
