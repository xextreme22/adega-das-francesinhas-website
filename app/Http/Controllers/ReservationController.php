<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use App\Http\Requests\ReservationUpdateRequest;
use App\Http\Requests\ReservationRequest;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\AndroidConfig;
use DateTimeZone;
use DateTime;
use DateInterval;
use Carbon\Carbon;

class ReservationController extends Controller
{
    private $firebase;
    private $firestore;
    private $storageBucket;
    private $messaging;

    public function __construct(){
        $this->firebase = (new Factory())->withServiceAccount(base_path().'\\FirebaseKey.json');
        $this->firestore = $this->firebase->createFirestore()->database();
        $this->storageBucket = $this->firebase->createStorage()->getBucket();
        $this->messaging = $this->firebase->createMessaging();
    }

    public function show($reservationId){
        $snapshotReservation = $this->firestore->collection('Reservas')->document($reservationId)->snapshot();
        if ($snapshotReservation->exists()) {
            $reservation = $snapshotReservation->data();
            $reservation['data'] = $snapshotReservation->data()['data']->get()->setTimezone(new DateTimeZone('Europe/Lisbon'))->format('Y-m-d H:i');

            //get order
            $documentsOrder = $this->firestore->collection('Reservas')->document($reservationId)->collection('Pedido')->documents();
            $order=null;
            $i=0;
            foreach ($documentsOrder as $documentOrder) {
                if($documentOrder->exists()){
                    $order[$i] = $documentOrder->data();
                    $order[$i]['id'] = $documentOrder->id();
                    $i++;
                }
            }

            //get promotions
            $documentsPromotions = $this->firestore->collection('Reservas')->document($reservationId)->collection('Promocoes')->documents();
            $promotions=null;
            foreach ($documentsPromotions as $documentPromotions) {
                if($documentPromotions->exists()){
                    $order[$i] = $documentPromotions->data();
                    $order[$i]['id'] = $documentPromotions->id();
                    $i++;
                }
            }

            return response()->json(['reservation'=>$reservation, 'order'=>$order], 200);
        } else {
            return response()->json("Invalid Reservation UID", 400);
        }
    }

    public function index(Request $request)
    {
        $page = $request->page;
        $pageSize = $request->pageSize;
        $query = $this->firestore->collection('Reservas')->offset($page*$pageSize)->limit($pageSize+1);

        //filter options
        if ($request->has('order')) {
            $query = $query->orderBy('data', $request->order);
        }else{
 
            if ($request->has('restaurant')) {
                $query = $query->where('restaurante', '=', $request->restaurant);
            }
    
            if ($request->has('phoneNumber')) {
                $query = $query->where('telemovel', '=', '+' . $request->phoneNumber); //+ is needed because you can't send it in url
            }
    
            if ($request->has('disabled')) {
                if($request->disabled){
                    $query = $query->where('cancelado', '=', true);
                }else{
                    $query = $query->where('cancelado', '=', false);
                }
            }

            if ($request->has('payed')) {
                if($request->payed){
                    $query = $query->where('pago', '=', true);
                }else{
                    $query = $query->where('pago', '=', false);
                }
            }
    
            if ($request->has('status')) {
                $query = $query->where('estado', '=', $request->status);
            }
        }

        $documents = $query->documents();
        $doc=[];
        $i=0;
        foreach ($documents as $document) {
            if($document->exists()){
                if($i != $pageSize){ //nao adiciona o ultimo eu so tou a ir buscar o ultimo para verificar se ainda existem mais documentos para saber se é a ultima pagina
                    $doc[$i] = $document->data();
                    $doc[$i]['id'] = $document->id();
                    $doc[$i]['data'] = $document->data()['data']->get()->setTimezone(new DateTimeZone('Europe/Lisbon'))->format('Y-m-d H:i');
                }
                $i++;
            }
        }

        if($documents->size() == $pageSize+1){
            $pagination['lastPage'] = false;
        }else{
            $pagination['lastPage'] = true;
        }

        return response()->json(['reservations'=>$doc, 'pagination'=>$pagination], 200);
    }

    /*
    public function indexToday(Request $request)
    {
        $today = new DateTime(Carbon::now());
        $today->sub(new DateInterval('PT1H'));
        $oneDay = new DateTime(Carbon::now());
        $oneDay->add(new DateInterval('P1D'));

        $documents = $this->firestore->collection('Reservas')
                    ->where('cancelado', '=', false)
                    ->where('data', '>', $today)
                    ->where('data', '<', $oneDay)
                    ->documents();

        $doc=null;
        $i=0;
        foreach ($documents as $document) {
            if($document->exists()){
                $doc[$i] = $document->data();
                $doc[$i]['id'] = $document->id();
                $doc[$i]['data'] = $document->data()['data']->get()->setTimezone(new DateTimeZone('Europe/Lisbon'))->format('Y-m-d H:i');
                $i++;
            }
        }

        return response()->json(['reservations'=>$doc], 200);
    }
    */

    public function store(ReservationRequest $request)
    {
        try{
            $date = new DateTime($request->data, new DateTimeZone('Europe/Lisbon'));
            $nomeRestaurante = strtolower($request->nomeRestaurante);
            $nomeUtilizador = strtolower($request->nome);

            $data = [
                'cancelado' => false,
                'numeroPessoas' => intval($request->numeroPessoas),
                'comentario' => $request->comentario,
                'data' => $date,
                'estado' => 'aceite',
                'pago' => false,
                'restaurante' => $nomeRestaurante,
                'utilizador' => null,
                'nome' => $nomeUtilizador,
                'telemovel' => "+" . $request->telemovel
            ];
            $storedData = $this->firestore->collection('Reservas')->add($data);
            $data['id'] = $storedData->id();
            $data['data'] = $storedData->snapshot()->data()["data"]->get()->format('Y-m-d H:i');
            return response()->json(['data'=>$data], 201);
        } catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function update(ReservationUpdateRequest $request, $reservationId)
    {
        $propertiesReservation = [
            ['path' => 'comentario', 'value' => $request->comentario],
            ['path' => 'numeroPessoas', 'value' => $request->numeroPessoas]
        ];

        $this->firestore->collection('Reservas')->document($reservationId)->update($propertiesReservation);
        $reservationUpdated = [
            'comentario' => $request->comentario,
            'numeroPessoas' => $request->numeroPessoas
        ];

        return response()->json(['reservation'=>$reservationUpdated], 200);
    }

    public function activate($reservationId)
    {
        try{
            $this->firestore->collection('Reservas')->document($reservationId)->update([
                ['path' => 'cancelado', 'value' => false]
            ]);
            return response()->json(['msg'=>'Reserva re-ativada'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function deactivate($reservationId)
    {
        try{
            $this->firestore->collection('Reservas')->document($reservationId)->update([
                ['path' => 'cancelado', 'value' => true]
            ]);
            return response()->json(['msg'=>'Reserva desativado'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function accept($reservationId)
    {
        try{
            $snapshot = $this->firestore->collection('Reservas')->document($reservationId)->snapshot();

            if ($snapshot->exists()) {
                if($snapshot->data()['utilizador'] === null){
                    return response()->json("Reserva não pode ser recusada pois não foi efetuada através da aplicação, nesta situação deve ligar para o cliente a infromar das alterações.", 400);
                }
                if($snapshot->data()['estado'] !== "em espera"){
                    return response()->json("Reserva não pode ser recusada pois já se encontra no estado:" . $snapshot->data()['estado'], 400);
                }

                $this->firestore->collection('Reservas')->document($reservationId)->update([
                    ['path' => 'estado', 'value' => "aceite"]
                ]);
            
                $this->sendNotification(
                    $snapshot->data()['utilizador'], 
                    'Reserva aceite', 
                    'Informamos que a sua reserva já foi aceite. Poderá fazer o seu pedido através da aplicação caso o deseje.'
                );

                return response()->json(['msg'=>'Reserva aceite'], 200);
            }else{
                return response()->json("Invalid Reservation id", 400);
            }
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function refuse($reservationId)
    {
        try{
            $snapshot = $this->firestore->collection('Reservas')->document($reservationId)->snapshot();

            if ($snapshot->exists()) {
                if($snapshot->data()['utilizador'] === null){
                    return response()->json("Reserva não pode ser recusada pois não foi efetuada através da aplicação, nesta situação deve ligar para o cliente e depois cancelar a reserva.", 400);
                }
                if($snapshot->data()['estado'] !== "em espera"){
                    return response()->json("Reserva não pode ser recusada pois já se encontra no estado:" . $snapshot->data()['estado'], 400);
                }

                $this->firestore->collection('Reservas')->document($reservationId)->update([
                    ['path' => 'estado', 'value' => "recusado"]
                ]);
            
                $this->sendNotification(
                    $snapshot->data()['utilizador'],
                    'Reserva recusada', 
                    'Infelizmente já não são aceites mais reservas para este dia.'
                );

                return response()->json(['msg'=>'Reserva recusada'], 200);
            }else{
                return response()->json("Invalid Reservation id", 400);
            }
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    private function sendNotification($topic, $title, $body){
        $config = AndroidConfig::fromArray([
            'ttl' => '3600s',
            'priority' => 'normal',
            'notification' => [
                'title' => $title,
                'body' => $body,
            ],
        ]);

        $message = CloudMessage::withTarget('topic', $topic)
            ->withAndroidConfig($config);
        $this->messaging->send($message);
    }
}
