<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use Kreait\Firebase\Exception\Auth\EmailExists;
use Kreait\Firebase\Exception\Auth\PhoneNumberExists;
use Kreait\Firebase\Exception\Auth\AuthError;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{

    private $firebase;
    private $auth;
    private $firestore;
    private $storageBucket;

    public function __construct(){
        $this->firebase = (new Factory())->withServiceAccount(base_path().'\\FirebaseKey.json');
        $this->auth = $this->firebase->createAuth();
        $this->firestore = $this->firebase->createFirestore()->database();
        $this->storageBucket = $this->firebase->createStorage()->getBucket();
    }

    private function createDirectoryIfDoesntExist(){
        $path = 'storage\\fotos\\utilizadores';
        $pathDirectory = public_path().'\\' . $path;
        File::makeDirectory($path, 0777, true, true);
    }

    public function index(Request $request)
    {
        $page = $request->page;
        $pageSize = $request->pageSize;
        $query = $this->firestore->collection('Utilizadores')->offset($page*$pageSize)->limit($pageSize+1);

        //filter options
        if ($request->has('order')) {
            $query = $query->orderBy('pontos', $request->order);
        }else{
            if ($request->has('name')) {
                $query = $query->where('nome', '=', $request->name);
            }
    
            if ($request->has('email')) {
                $query = $query->where('email', '=', $request->email);
            }
    
            if ($request->has('phoneNumber')) {
                $query = $query->where('telemovel', '=', '+' . $request->phoneNumber); //+ is needed because you can't send it in url
            }
    
            if ($request->has('disabled')) {
                if($request->disabled){
                    $query = $query->where('disabled', '=', true);
                }else{
                    $query = $query->where('disabled', '=', false);
                }
            }
    
            if ($request->has('role')) {
                $query = $query->where('role', '=', $request->role);
            }
        }

        $documents = $query->documents();
        $this->createDirectoryIfDoesntExist();
        $doc=[];
        $i=0;
        foreach ($documents as $document) {
            if($document->exists()){
                if($i != $pageSize){ //nao adiciona o ultimo eu so tou a ir buscar o ultimo para verificar se ainda existem mais documentos para saber se é a ultima pagina
                    $doc[$i] = $document->data();
                    $doc[$i]['uid'] = $document->id();
                    $image = 'storage\\fotos\\utilizadores\\'.$document->id().'.jpg';
                    $doc[$i]['photo'] = '\\'.$image;

                    $object = $this->storageBucket->object('utilizadores/'.$document->id().'/avatar.jpg');
                    if (!file_exists( public_path() . '\\'.$image)) {
                        if($object->exists()){
                            $object->downloadToFile($image);
                        }else{
                            $doc[$i]['photo'] = '\\static\\placeholder.jpg';
                        }
                    }
                }
                $i++;
            }
        }

        if($documents->size() == $pageSize+1){
            $pagination['lastPage'] = false;
        }else{
            $pagination['lastPage'] = true;
        }

        return response()->json(['users'=>$doc, 'pagination'=>$pagination], 200);
    }

    public function update(UserUpdateRequest $request, $userId)
    {
        if($request->photo !== null){
            if (strpos($request->input('photo'), 'data:image/') !== false) {
                $exploded = explode(',', $request->photo);
                $image = base64_decode($exploded[1]);
                if (!Str::contains($exploded[0], 'jpeg') && !Str::contains($exploded[0], 'jpg')) {
                    return response()->json(['errors'=>array("photo" => "Error incorrect Photo extension (only accepts jpg or jpeg)")], 422);
                }
                if(strlen($image) > 200000){
                    return response()->json(['errors'=>array("photo" => "Image size cannot exceed 200KB for perfomance reasons on mobile phones")], 422);
                }
                
                $name_image='utilizadores/'.$userId."/avatar.jpg";
                $this->storageBucket->upload(
                    $image, 
                    ['name' => $name_image]
                );

                $object = $this->storageBucket->object($name_image);
                $path = 'storage/fotos/utilizadores/'.$userId.'.jpg';
                $data['photo'] = $path;
                $this->createDirectoryIfDoesntExist();
                $object->downloadToFile($path);
            }else{
                return response()->json(['errors'=>array("photo" => "Photo does not corresponde to an image file")], 422);
            }
        }else{
            $data['photo'] = '\\static\\placeholder.jpg';
        }

        $propertiesAuth = [
            'displayName' => $request->nome,
            'phoneNumber' => $request->telemovel
        ];

        $this->auth->setCustomUserAttributes($userId, ['role' => $request->role]);

        $propertiesUser = [
            ['path' => 'nome', 'value' => $request->nome],
            ['path' => 'telemovel', 'value' => $request->telemovel],
            ['path' => 'role', 'value' => $request->role],
            ['path' => 'pontos', 'value' => $request->pontos],
        ];

        if($request->password){
            $this->auth->changeUserPassword($userId, $request->password);
        }

        $this->firestore->collection('Utilizadores')->document($userId)->update($propertiesUser);
        $updatedUser = $this->auth->updateUser($userId, $propertiesAuth);
        return response()->json(['user'=>$data], 200);
    }

    public function store(UserRequest $request)
    {
        try{
            $userProperties = [
                'email' => $request->email,
                'emailVerified' => false,
                'phoneNumber' => $request->telemovel,
                'password' => $request->password,
                'displayName' => $request->nome,
                'disabled' => false,
                'customAttributes' => ['role' => $request->role]
            ];

            $createdUser = $this->auth->createUser($userProperties);
            $this->auth->setCustomUserAttributes($createdUser->uid, ['role' => $request->role]);
            
            $data = [
                'email' => $createdUser->email,
                'telemovel' => $createdUser->phoneNumber,
                'nome' => $createdUser->displayName,
                'role' => $request->role,
                'pontos' => 0,
                'disabled' => false,
            ];
            $this->firestore->collection('Utilizadores')->document($createdUser->uid)->set($data);
            $this->auth->sendEmailVerificationLink($request->email);
            $data['uid'] = $createdUser->uid;

            if($request->photo !== null){
                if (strpos($request->input('photo'), 'data:image/') !== false) {
                    $exploded = explode(',', $request->photo);
                    $image = base64_decode($exploded[1]);
                    if (!Str::contains($exploded[0], 'jpeg') && !Str::contains($exploded[0], 'jpg')) {
                        return response()->json(['errors'=>array("photo" => "Error incorrect Photo extension (only accepts jpg or jpeg)")], 422);
                    }
                    if(strlen($image) > 200000){
                        return response()->json(['errors'=>array("photo" => "Image size cannot exceed 200KB for perfomance reasons on mobile phones")], 422);
                    }
                    
                    $name_image='utilizadores/'.$createdUser->uid."/avatar.jpg";
                    $this->storageBucket->upload(
                        $image, 
                        ['name' => $name_image]
                    );

                    $object = $this->storageBucket->object($name_image);
                    $path = 'storage/fotos/utilizadores/'.$createdUser->uid.'.jpg';
                    $data['photo'] = $path;
                    $this->createDirectoryIfDoesntExist();
                    $object->downloadToFile($path);
                }else{
                    return response()->json(['errors'=>array("photo" => "Photo does not corresponde to an image file")], 422);
                }
            }else{
                $data['photo'] = '\\static\\placeholder.jpg';
            }

            return response()->json(['data'=>$data], 201);
        } catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        } catch (EmailExists $e) {
            return response()->json(['errors'=>array("email" => "Email introduzido já existe")], 422);
        } catch (PhoneNumberExists $e){
            return response()->json(['errors'=>array("telemovel" => "Telemovel introduzido já existe")], 422);
        } catch (AuthError $e){
            return response()->json(['errors'=>array("telemovel" => "telemovel invalido")], 422);
        } 
    }

    public function activate($userId)
    {
        try{
            $this->firestore->collection('Utilizadores')->document($userId)->update([
                ['path' => 'disabled', 'value' => false]
            ]);
            $updatedUser = $this->auth->enableUser($userId);
            return response()->json(['msg'=>'Utilizador ativado'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function deactivate($userId)
    {
        try{
            $this->firestore->collection('Utilizadores')->document($userId)->update([
                ['path' => 'disabled', 'value' => true]
            ]);
            $updatedUser = $this->auth->disableUser($userId);
            return response()->json(['msg'=>'Utilizador desativado'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }
}
