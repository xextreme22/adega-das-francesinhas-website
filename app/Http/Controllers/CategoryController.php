<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use App\Http\Requests\CategoryRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;

class CategoryController extends Controller
{
    private $firebase;
    private $firestore;
    private $storageBucket;

    public function __construct(){
        $this->firebase = (new Factory())->withServiceAccount(base_path().'\\FirebaseKey.json');
        $this->firestore = $this->firebase->createFirestore()->database();
        $this->storageBucket = $this->firebase->createStorage()->getBucket();
    }

    private function createDirectoryIfDoesntExist($path){
        $pathDirectory = public_path().'\\' . $path;
        File::makeDirectory($path, 0777, true, true);
    }

    public function store(CategoryRequest $request, $restaurantId)
    {
        try{
            /*
            $nomeRestaurant = strtolower($restaurantId); 
            $nomeCategoria = strtolower($request->nome); 
            */
            $data = [
                'disabled' => false,
            ];
            $storedData = $this->firestore->collection('Restaurantes')->document($restaurantId)
                                            ->collection('Categorias')->document($request->nome)
                                            ->set($data);
            $data['nome'] = $request->nome;
            return response()->json(['data'=>$data], 201);
        } catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function activate($restaurantId, $categoryId)
    {
        try{
            /*
            $restaurantId = strtolower($restaurantId);
            $categoryId = strtolower($categoryId);
            */

            $this->firestore->collection('Restaurantes')->document($restaurantId)->collection('Categorias')->document($categoryId)->update([
                ['path' => 'disabled', 'value' => false]
            ]);
            return response()->json(['msg'=>'Categoria ativada'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function deactivate($restaurantId, $categoryId)
    {
        try{
            /*
            $restaurantId = strtolower($restaurantId);
            $categoryId = strtolower($categoryId);
            */

            $this->firestore->collection('Restaurantes')->document($restaurantId)->collection('Categorias')->document($categoryId)->update([
                ['path' => 'disabled', 'value' => true]
            ]);
            return response()->json(['msg'=>'Categoria desativada'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function menus(Request $request, $restaurantId, $categoryId)
    {
        /*
        $nomeRestaurant = strtolower($nomeRestaurant);
        $nomeCategory = strtolower($nomeCategory);
        */
        $documents = $this->firestore->collection('Restaurantes')->document($restaurantId)
                                    ->collection('Categorias')->document($categoryId)
                                    ->collection('Items')->documents();

        $doc=null;
        $i=0;
        $this->createDirectoryIfDoesntExist('storage\\fotos\\restaurantes\\menu');
        foreach ($documents as $document) {
            if($document->exists()){
                $doc[$i] = $document->data();
                $doc[$i]['nome'] = $document->id();
                $image = 'storage\\fotos\\restaurantes\\menu\\'.$document->id().'.jpg';
                $doc[$i]['photo'] = '\\'.$image;

                if (!file_exists( public_path() . '\\'.$image)) {
                    $object = $this->storageBucket->object('restaurantes/menu/'.$document->id().'.jpg');
                    if($object->exists()){
                        $object->downloadToFile($image);
                    }else{
                        $doc[$i]['photo'] = '\\static\\placeholder.jpg';
                    }
                }
                $i++;
            }
        }

        return response()->json(['menus'=>$doc], 200);
    }

    public function menusSimplified(Request $request, $restaurantId, $categoryId)
    {
        /*
        $nomeRestaurant = strtolower($nomeRestaurant);
        $nomeCategory = strtolower($nomeCategory);
        */
        $documents = $this->firestore->collection('Restaurantes')->document($restaurantId)
                                    ->collection('Categorias')->document($categoryId)
                                    ->collection('Items')->documents();

        $doc=null;
        $i=0;
        foreach ($documents as $document) {
            if($document->exists()){
                $doc[$i]['nome'] = $document->id();
                $i++;
            }
        }

        return response()->json(['names'=>$doc], 200);
    }
}
