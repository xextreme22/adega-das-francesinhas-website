<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Google\Cloud\Firestore\FieldValue;
use App\Http\Requests\AllergenRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;

class AllergenController extends Controller
{
    private $firebase;
    private $firestore;
    private $storageBucket;

    public function __construct(){
        $this->firebase = (new Factory())->withServiceAccount(base_path().'\\FirebaseKey.json');
        $this->firestore = $this->firebase->createFirestore()->database();
        $this->storageBucket = $this->firebase->createStorage()->getBucket();
    }

    private function createDirectoryIfDoesntExist(){
        $path = 'storage\\fotos\\restaurantes\\menu\\alergenicos';
        $pathDirectory = public_path().'\\' . $path;
        File::makeDirectory($path, 0777, true, true);
    }

    public function store(AllergenRequest $request, $restaurantId, $categoryId, $menuId)
    {
        try{
            /*
            $nomeRestaurant = strtolower($nomeRestaurant);
            $nomeCategory = strtolower($nomeCategory);
            $nomeItem = strtolower($nomeItem);
            */
            if($request->photo !== null){
                if (strpos($request->input('photo'), 'data:image/') !== false) {
                    $exploded = explode(',', $request->photo);
                    $image = base64_decode($exploded[1]);
                    if (!Str::contains($exploded[0], 'png')) {
                        return response()->json(['errors'=>array("photo" => "Error incorrect Photo extension (only accepts png)")], 422);
                    }
                    if(strlen($image) > 200000){
                        return response()->json(['errors'=>array("photo" => "Image size cannot exceed 200KB for perfomance reasons on mobile phones")], 422);
                    }
                    
    
                }else{
                    return response()->json(['errors'=>array("photo" => "Photo does not corresponde to an image file")], 422);
                }
            }else{
                return response()->json(['errors'=>array("photo" => "Imagem é obrigatória")], 422);
            }

            $data = [
                'nome' => $request->nome,
            ];

            $storedData = $this->firestore->collection('Restaurantes')->document($restaurantId)
                                        ->collection('Categorias')->document($categoryId)
                                        ->collection('Items')->document($menuId)
                                        ->collection('Alergenicos')->add($data);
            $id = $storedData->id();

            $name_image='restaurantes/menu/alergenicos/'.$id.".png";
            $this->storageBucket->upload(
                $image, 
                ['name' => $name_image]
            );
            $object = $this->storageBucket->object('restaurantes/menu/alergenicos/'.$id.'.png');
            $path = 'storage/fotos/restaurantes/menu/alergenicos/'.$id.'.png';
            $this->createDirectoryIfDoesntExist();
            $object->downloadToFile($path);

            $data['photo'] = $path;
            $data['id'] = $id;
            return response()->json(['data'=>$data], 201);
        } catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function update(AllergenRequest $request, $restaurantId, $categoryId, $menuId, $allergenId)
    {
        /*
        $nomeRestaurant = strtolower($nomeRestaurant);
        $nomeCategory = strtolower($nomeCategory);
        $nomeItem = strtolower($nomeItem);
        */
        $itemUpdated = [];

        if($request->photo !== null){
            if (strpos($request->input('photo'), 'data:image/') !== false) {
                $exploded = explode(',', $request->photo);
                $image = base64_decode($exploded[1]);
                if (!Str::contains($exploded[0], 'png')) {
                    return response()->json(['errors'=>array("photo" => "Error incorrect Photo extension (only accepts png)")], 422);
                }
                if(strlen($image) > 200000){
                    return response()->json(['errors'=>array("photo" => "Image size cannot exceed 200KB for perfomance reasons on mobile phones")], 422);
                }
                
                $name_image='restaurantes/menu/alergenicos/'.$allergenId.'.png';
                $this->storageBucket->upload(
                    $image, 
                    ['name' => $name_image]
                );
                $object = $this->storageBucket->object('restaurantes/menu/alergenicos/'.$allergenId.'.png');
                $path = 'storage/fotos/restaurantes/menu/alergenicos/'.$allergenId.'.png';
                $this->createDirectoryIfDoesntExist();
                $object->downloadToFile($path);

            }else{
                return response()->json(['errors'=>array("photo" => "Photo does not corresponde to an image file")], 422);
            }
        }

        $properties = [
            ['path' => 'nome', 'value' => $request->nome],
        ];

        $this->firestore->collection('Restaurantes')->document($restaurantId)
                        ->collection('Categorias')->document($categoryId)
                        ->collection('Items')->document($menuId)
                        ->collection('Alergenicos')->document($allergenId)
                        ->update($properties);

        $itemUpdated['nome'] = $request->nome;
        $itemUpdated['photo'] = 'storage/fotos/restaurantes/menu/alergenicos/'.$allergenId.'.png';

        return response()->json(['allergen'=>$itemUpdated], 200);
    }

    public function activate($restaurantId, $categoryId, $menuId, $allergenId)
    {
        try{
            $this->firestore->collection('Restaurantes')->document($restaurantId)
                            ->collection('Categorias')->document($categoryId)
                            ->collection('Items')->document($menuId)
                            ->collection('Alergenicos')->document($allergenId)
                            ->update([
                                ['path' => 'disabled', 'value' => false]
                            ]);
            return response()->json(['msg'=>'Alergénico ativado'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function deactivate($restaurantId, $categoryId, $menuId, $allergenId)
    {
        try{
            $this->firestore->collection('Restaurantes')->document($restaurantId)
                            ->collection('Categorias')->document($categoryId)
                            ->collection('Items')->document($menuId)
                            ->collection('Alergenicos')->document($allergenId)
                            ->update([
                                ['path' => 'disabled', 'value' => true]
                            ]);
            return response()->json(['msg'=>'Alergénico desativado'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }
}
