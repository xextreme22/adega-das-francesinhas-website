<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use App\Http\Requests\MenuRequest;
use App\Http\Requests\MenuUpdateRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;

class MenusController extends Controller
{
    private $firebase;
    private $firestore;
    private $storageBucket;

    public function __construct(){
        $this->firebase = (new Factory())->withServiceAccount(base_path().'\\FirebaseKey.json');
        $this->firestore = $this->firebase->createFirestore()->database();
        $this->storageBucket = $this->firebase->createStorage()->getBucket();
    }

    private function createDirectoryIfDoesntExist($path){
        $pathDirectory = public_path().'\\' . $path;
        File::makeDirectory($path, 0777, true, true);
    }

    public function store(MenuRequest $request, $restaurantId, $categoryId)
    {
        try{
            /*
            $nomeRestaurant = strtolower($nomeRestaurant);
            $nomeCategory = strtolower($nomeCategory);
            $nome = strtolower($request->nome);
            */
            if($request->photo !== null){
                if (strpos($request->input('photo'), 'data:image/') !== false) {
                    $exploded = explode(',', $request->photo);
                    $image = base64_decode($exploded[1]);
                    if (!Str::contains($exploded[0], 'jpeg') && !Str::contains($exploded[0], 'jpg')) {
                        return response()->json(['errors'=>array("photo" => "Error incorrect Photo extension (only accepts jpg or jpeg)")], 422);
                    }
                    if(strlen($image) > 200000){
                        return response()->json(['errors'=>array("photo" => "Image size cannot exceed 200KB for perfomance reasons on mobile phones")], 422);
                    }
                    
                    $name_image='restaurantes/menu/'.$request->nome.".jpg";
                    $this->storageBucket->upload(
                        $image, 
                        ['name' => $name_image]
                    );
                }else{
                    return response()->json(['errors'=>array("photo" => "Photo does not corresponde to an image file")], 422);
                }
            }else{
                return response()->json(['errors'=>array("photo" => "Imagem é obrigatória")], 422);
            }

            $data = [
                'ingredientes' => $request->ingredientes,
                'preco' => floatval(number_format((float)$request->preco, 2, '.', '')),
                'disabled' => false,
            ];

            $storedData = $this->firestore->collection('Restaurantes')->document($restaurantId)
                                        ->collection('Categorias')->document($categoryId)
                                        ->collection('Items')->document($request->nome)->set($data);


            $object = $this->storageBucket->object('restaurantes/menu/'.$request->nome.'.jpg');
            $path = 'storage/fotos/restaurantes/menu/'.$request->nome.'.jpg';
            $this->createDirectoryIfDoesntExist('storage\\fotos\\restaurantes\\menu');
            $object->downloadToFile($path);

            $data['photo'] = $path;
            $data['nome'] = $request->nome;
            return response()->json(['data'=>$data], 201);
        } catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function update(MenuUpdateRequest $request, $restaurantId, $categoryId, $menuId)
    {
        /*
        $nomeRestaurant = strtolower($nomeRestaurant);
        $nomeCategory = strtolower($nomeCategory);
        $nomeItem = strtolower($nomeItem);
        */
        if($request->photo !== null){
            if (strpos($request->input('photo'), 'data:image/') !== false) {
                $exploded = explode(',', $request->photo);
                $image = base64_decode($exploded[1]);
                if (!Str::contains($exploded[0], 'jpeg') && !Str::contains($exploded[0], 'jpg')) {
                    return response()->json(['errors'=>array("photo" => "Error incorrect Photo extension (only accepts jpg or jpeg)")], 422);
                }
                if(strlen($image) > 200000){
                    return response()->json(['errors'=>array("photo" => "Image size cannot exceed 200KB for perfomance reasons on mobile phones")], 422);
                }
                
                $name_image='restaurantes/menu/'.$menuId.'.jpg';
                $this->storageBucket->upload(
                    $image, 
                    ['name' => $name_image]
                );

                $object = $this->storageBucket->object('restaurantes/menu/'.$menuId.'.jpg');
                $path = 'storage/fotos/restaurantes/menu/'.$menuId.'.jpg';
                $this->createDirectoryIfDoesntExist('storage\\fotos\\restaurantes\\menu');
                $object->downloadToFile($path);
            }else{
                return response()->json(['errors'=>array("photo" => "Photo does not corresponde to an image file")], 422);
            }
        }
        
        $preco = floatval(number_format((float)$request->preco, 2, '.', ''));
        $propertiesRestaurant = [
            ['path' => 'ingredientes', 'value' => $request->ingredientes],
            ['path' => 'preco', 'value' => $preco],
        ];

        $this->firestore->collection('Restaurantes')->document($restaurantId)
                        ->collection('Categorias')->document($categoryId)
                        ->collection('Items')->document($menuId)
                        ->update($propertiesRestaurant);

        $itemUpdated = [
            'ingredientes' => $request->ingredientes,
            'preco' => $preco,
            'photo' => 'storage/fotos/restaurantes/menu/'.$menuId.'.jpg'
        ];

        return response()->json(['menu'=>$itemUpdated], 200);
    }

    public function activate($restaurantId, $categoryId, $menuId)
    {
        try{
            /*
            $nomeRestaurant = strtolower($nomeRestaurant);
            $nomeCategory = strtolower($nomeCategory);
            $nomeItem = strtolower($nomeItem);
            */
            $this->firestore->collection('Restaurantes')->document($restaurantId)
                            ->collection('Categorias')->document($categoryId)
                            ->collection('Items')->document($menuId)
                            ->update([
                                ['path' => 'disabled', 'value' => false]
                            ]);
            return response()->json(['msg'=>'Menu ativado'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function deactivate($restaurantId, $categoryId, $menuId)
    {
        try{
            /*
            $nomeRestaurant = strtolower($nomeRestaurant);
            $nomeCategory = strtolower($nomeCategory);
            $nomeItem = strtolower($nomeItem);
            */
            $this->firestore->collection('Restaurantes')->document($restaurantId)
                            ->collection('Categorias')->document($categoryId)
                            ->collection('Items')->document($menuId)
                            ->update([
                                ['path' => 'disabled', 'value' => true]
                            ]);
            return response()->json(['msg'=>'Menu desativado'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function allergens(Request $request, $restaurantId, $categoryId, $menuId)
    {
        $this->createDirectoryIfDoesntExist('storage\\fotos\\restaurantes\\menu\\alergenicos');

        /*
        $nomeRestaurant = strtolower($nomeRestaurant);
        $nomeCategory = strtolower($nomeCategory);
        $nomeItem = strtolower($nomeItem);
        */
        $documents = $this->firestore->collection('Restaurantes')->document($restaurantId)
                                    ->collection('Categorias')->document($categoryId)
                                    ->collection('Items')->document($menuId)
                                    ->collection('Alergenicos')->documents();

        $doc=null;
        $i=0;
        foreach ($documents as $document) {
            if($document->exists()){
                $doc[$i] = $document->data();
                $doc[$i]['id'] = $document->id();
                $image = 'storage\\fotos\\restaurantes\\menu\\alergenicos\\'.$document->id().'.png';
                $doc[$i]['photo'] = '\\'.$image;
                $i++;

                if (!file_exists( public_path() . '\\'.$image)) {
                    $object = $this->storageBucket->object('restaurantes/menu/alergenicos/'.$document->id().'.png');
                    if($object->exists()){
                        $object->downloadToFile($image);
                    }
                }
            }
        }

        return response()->json(['allergens'=>$doc], 200);
    }
}
