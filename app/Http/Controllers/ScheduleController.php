<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use App\Http\Requests\ScheduleRequest;
use App\Http\Requests\ScheduleUpdateRequest;
use App\Http\Requests\ScheduleExceptionRequest;
use App\Http\Requests\ScheduleExceptionUpdateRequest;
use DateTimeZone;
use DateTime;
use DateInterval;

class ScheduleController extends Controller
{
    private $firebase;
    private $firestore;
    private $storageBucket;

    public function __construct(){
        $this->firebase = (new Factory())->withServiceAccount(base_path().'\\FirebaseKey.json');
        $this->firestore = $this->firebase->createFirestore()->database();
        $this->storageBucket = $this->firebase->createStorage()->getBucket();
    }

    public function store(ScheduleRequest $request, $restaurantId)
    {
        try{
            $horaAbertura = new DateTime($request->horaAbertura);
            $horaAbertura = $this->roundTimeSameDay($horaAbertura);
            $horaAbertura = $horaAbertura->format('H') . $horaAbertura->format('i');

            $horaFecho = new DateTime($request->horaFecho);
            $horaFecho = $this->roundTimeSameDay($horaFecho);
            $horaFecho = $horaFecho->format('H') . $horaFecho->format('i');

            if(intval($horaAbertura) >= intval($horaFecho)){
                return response()->json(['errors'=>array("horaFecho" => "Hora de fecho tem de ser maior que a hora de abertura")], 422);
            }

            $data = [
                'diaSemana' => $request->diaSemana,
                'horaAbertura' => $horaAbertura,
                'horaFecho' => $horaFecho,
                'aceitaReservas' => $request->aceitaReservas,
                'aceitaTakeaways' => $request->aceitaTakeaways,
                'disabled' => false,
            ];
            $storedData = $this->firestore->collection('Restaurantes')->document($restaurantId)->collection('Horarios')->add($data);
            $data['id'] = $storedData->id();
            $data['horaAbertura'] = DateTime::createFromFormat('Hi', $storedData->snapshot()->data()["horaAbertura"])->format('H:i');
            $data['horaFecho'] = DateTime::createFromFormat('Hi', $storedData->snapshot()->data()["horaFecho"])->format('H:i');
            return response()->json(['data'=>$data], 201);
        } catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    private function roundTimeSameDay($time){
        $hour = $time->format('H');
        $minute = $time->format('i');
        
        //nao dar round para 00:00
        if(intval($hour) === 23){
            if(intval($minute) > 55){
                $time->setTime( $time->format("H"), "55");
                return $time;
            }
        }

        return $this->roundTime($time);
    }

    private function roundTime($time){
        $minute = $time->format('i');

        $aux = 5 * round(intval($minute) / 5);

        $minuteRest = $minute % 5;
        if($minuteRest != 0)
        {
            if($aux > $minute){
                while((intval($time->format('i')) % 5) !== 0){
                    $time->add(new DateInterval("PT1M"));
                }
            }
            if($aux < $minute){
                while((intval($time->format('i')) % 5) !== 0){
                    $time->sub(new DateInterval("PT1M"));
                }
            }
        }
        return $time;
    }

    public function update(ScheduleUpdateRequest $request, $restaurantId, $scheduleId)
    {
        $horaAbertura = new DateTime($request->horaAbertura);
        $horaAbertura = $this->roundTimeSameDay($horaAbertura);
        $horaAbertura = $horaAbertura->format('H') . $horaAbertura->format('i');

        $horaFecho = new DateTime($request->horaFecho);
        $horaFecho = $this->roundTimeSameDay($horaFecho);
        $horaFecho = $horaFecho->format('H') . $horaFecho->format('i');

        if(intval($horaAbertura) >= intval($horaFecho)){
            return response()->json(['errors'=>array("horaFecho" => "Hora de fecho tem de ser maior que a hora de abertura")], 422);
        }

        $propertiesSchedule = [
            ['path' => 'diaSemana', 'value' => $request->diaSemana],
            ['path' => 'aceitaTakeaways', 'value' => $request->aceitaTakeaways],
            ['path' => 'aceitaReservas', 'value' => $request->aceitaReservas],
            ['path' => 'horaAbertura', 'value' => $horaAbertura],
            ['path' => 'horaFecho', 'value' => $horaFecho],
        ];

        $this->firestore->collection('Restaurantes')->document($restaurantId)
                        ->collection('Horarios')->document($scheduleId)
                        ->update($propertiesSchedule);

        $scheduleUpdated = [
            'diaSemana' => $request->diaSemana,
            'aceitaTakeaways' => $request->aceitaTakeaways,
            'aceitaReservas' => $request->aceitaReservas,
            'horaAbertura' => DateTime::createFromFormat('Hi', $horaAbertura)->format('H:i'),
            'horaFecho' => DateTime::createFromFormat('Hi', $horaFecho)->format('H:i'),
        ];

        return response()->json(['schedule'=>$scheduleUpdated], 200);
    }

    public function activate($restaurantId, $scheduleId)
    {
        try{
            $this->firestore->collection('Restaurantes')->document($restaurantId)
                            ->collection('Horarios')->document($scheduleId)
                            ->update([
                                ['path' => 'disabled', 'value' => false]
                            ]);
            return response()->json(['msg'=>'Horario ativado'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function deactivate($restaurantId, $scheduleId)
    {
        try{
            $this->firestore->collection('Restaurantes')->document($restaurantId)
                            ->collection('Horarios')->document($scheduleId)
                            ->update([
                                ['path' => 'disabled', 'value' => true]
                            ]);
            return response()->json(['msg'=>'Horario desativado'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function storeException(ScheduleExceptionRequest $request, $restaurantId)
    {
        try{
            $dataInicio = new DateTime($request->dataInicio, new DateTimeZone('Europe/Lisbon'));
            $dataInicio = $this->roundTime($dataInicio);

            $dataFim = new DateTime($request->dataFim, new DateTimeZone('Europe/Lisbon'));
            $dataFim = $this->roundTime($dataFim);

            if($dataInicio >= $dataFim){
                return response()->json(['errors'=>array("dataFim" => "Data de fim tem de ser maior que a data de inicio")], 422);
            }

            $data = [
                'dataInicio' => $dataInicio,
                'dataFim' => $dataFim,
                'aceitaReservas' => $request->aceitaReservas,
                'aceitaTakeaways' => $request->aceitaTakeaways,
                'disabled' => false,
            ];
            $storedData = $this->firestore->collection('Restaurantes')->document($restaurantId)->collection('ExcecoesHorario')->add($data);
            $data['id'] = $storedData->id();
            $data['dataInicio'] = $storedData->snapshot()->data()["dataInicio"]->get()->setTimezone(new DateTimeZone('Europe/Lisbon'))->format('Y-m-d H:i');
            $data['dataFim'] = $storedData->snapshot()->data()["dataFim"]->get()->setTimezone(new DateTimeZone('Europe/Lisbon'))->format('Y-m-d H:i');
            return response()->json(['data'=>$data], 201);
        } catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function updateException(ScheduleExceptionUpdateRequest $request, $restaurantId, $exceptionId)
    {
        $propertiesSchedule = [
            ['path' => 'aceitaTakeaways', 'value' => $request->aceitaTakeaways],
            ['path' => 'aceitaReservas', 'value' => $request->aceitaReservas],
        ];

        $this->firestore->collection('Restaurantes')->document($restaurantId)
                        ->collection('ExcecoesHorario')->document($exceptionId)
                        ->update($propertiesSchedule);

        $scheduleUpdated = [
            'aceitaTakeaways' => $request->aceitaTakeaways,
            'aceitaReservas' => $request->aceitaReservas,
        ];

        return response()->json(['schedule'=>$scheduleUpdated], 200);
    }

    public function activateException($restaurantId, $exceptionId)
    {
        try{
            $this->firestore->collection('Restaurantes')->document($restaurantId)
                            ->collection('ExcecoesHorario')->document($exceptionId)
                            ->update([
                                ['path' => 'disabled', 'value' => false]
                            ]);
            return response()->json(['msg'=>'Exceção ao horário ativada'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function deactivateException($restaurantId, $exceptionId)
    {
        try{
            $this->firestore->collection('Restaurantes')->document($restaurantId)
                            ->collection('ExcecoesHorario')->document($exceptionId)
                            ->update([
                                ['path' => 'disabled', 'value' => true]
                            ]);
            return response()->json(['msg'=>'Exceção ao horário desativada'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }
}
