<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use App\Http\Requests\TakeawayUpdateRequest;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\AndroidConfig;
use DateTimeZone;
use DateTime;
use DateInterval;
use Carbon\Carbon;

class TakeawayController extends Controller
{
    private $firebase;
    private $firestore;
    private $storageBucket;
    private $messaging;

    public function __construct(){
        $this->firebase = (new Factory())->withServiceAccount(base_path().'\\FirebaseKey.json');
        $this->firestore = $this->firebase->createFirestore()->database();
        $this->storageBucket = $this->firebase->createStorage()->getBucket();
        $this->messaging = $this->firebase->createMessaging();
    }

    public function show($takeawayId){
        $snapshotTakeaway = $this->firestore->collection('Takeaways')->document($takeawayId)->snapshot();
        if ($snapshotTakeaway->exists()) {
            $takeaway = $snapshotTakeaway->data();
            $takeaway['data'] = $snapshotTakeaway->data()['data']->get()->setTimezone(new DateTimeZone('Europe/Lisbon'))->format('Y-m-d H:i');

            //get order
            $documentsOrder = $this->firestore->collection('Takeaways')->document($takeawayId)->collection('Pedido')->documents();
            $order=null;
            $i=0;
            foreach ($documentsOrder as $documentOrder) {
                if($documentOrder->exists()){
                    $order[$i] = $documentOrder->data();
                    $order[$i]['id'] = $documentOrder->id();
                    $i++;
                }
            }

            //get promotions
            $documentsPromotions = $this->firestore->collection('Takeaways')->document($takeawayId)->collection('Promocoes')->documents();
            $promotions=null;
            foreach ($documentsPromotions as $documentPromotions) {
                if($documentPromotions->exists()){
                    $order[$i] = $documentPromotions->data();
                    $order[$i]['id'] = $documentPromotions->id();
                    $i++;
                }
            }

            return response()->json(['takeaway'=>$takeaway, 'order'=>$order], 200);
        } else {
            return response()->json("Invalid Takeaway UID", 500);
        }
    }

    public function index(Request $request)
    {
        $page = $request->page;
        $pageSize = $request->pageSize;
        $query = $this->firestore->collection('Takeaways')->offset($page*$pageSize)->limit($pageSize+1);

        //filter options
        if ($request->has('order')) {
            $query = $query->orderBy('data', $request->order);
        }else{
 
            if ($request->has('restaurant')) {
                $query = $query->where('restaurante', '=', $request->restaurant);
            }
    
            if ($request->has('phoneNumber')) {
                $query = $query->where('telemovel', '=', '+' . $request->phoneNumber); //+ is needed because you can't send it in url
            }
    
            if ($request->has('disabled')) {
                if($request->disabled){
                    $query = $query->where('cancelado', '=', true);
                }else{
                    $query = $query->where('cancelado', '=', false);
                }
            }

            if ($request->has('payed')) {
                if($request->payed){
                    $query = $query->where('pago', '=', true);
                }else{
                    $query = $query->where('pago', '=', false);
                }
            }
    
            if ($request->has('status')) {
                $query = $query->where('estado', '=', $request->status);
            }
        }

        $documents = $query->documents();
        $doc=[];
        $i=0;
        foreach ($documents as $document) {
            if($document->exists()){
                if($i != $pageSize){ //nao adiciona o ultimo eu so tou a ir buscar o ultimo para verificar se ainda existem mais documentos para saber se é a ultima pagina
                    $doc[$i] = $document->data();
                    $doc[$i]['id'] = $document->id();
                    $doc[$i]['data'] = $document->data()['data']->get()->setTimezone(new DateTimeZone('Europe/Lisbon'))->format('Y-m-d H:i');
                }
                $i++;
            }
        }

        if($documents->size() == $pageSize+1){
            $pagination['lastPage'] = false;
        }else{
            $pagination['lastPage'] = true;
        }

        return response()->json(['takeaways'=>$doc, 'pagination'=>$pagination], 200);
    }

    /*
    public function indexToday(Request $request)
    {
        $today = new DateTime(Carbon::now());
        $today->sub(new DateInterval('PT1H'));
        $oneDay = new DateTime(Carbon::now());
        $oneDay->add(new DateInterval('P1D'));

        $documents = $this->firestore->collection('Takeaways')
                    ->where('pago', '=', true)
                    ->where('cancelado', '=', false)
                    ->where('data', '>', $today)
                    ->where('data', '<', $oneDay)
                    ->documents();

        $doc=null;
        $i=0;
        foreach ($documents as $document) {
            if($document->exists()){
                $doc[$i] = $document->data();
                $doc[$i]['id'] = $document->id();
                $doc[$i]['data'] = $document->data()['data']->get()->setTimezone(new DateTimeZone('Europe/Lisbon'))->format('Y-m-d H:i');
                $i++;
            }
        }

        return response()->json(['takeaways'=>$doc], 200);
    }
    */

    public function update(TakeawayUpdateRequest $request, $takeawayId)
    {
        $propertiesTakeaway = [
            ['path' => 'comentario', 'value' => $request->comentario]
        ];

        $this->firestore->collection('Takeaways')->document($takeawayId)->update($propertiesTakeaway);
        $takeawayUpdated = [
            'comentario' => $request->comentario,
        ];

        return response()->json(['takeaway'=>$takeawayUpdated], 200);
    }

    public function activate($takeawayId)
    {
        try{
            $this->firestore->collection('Takeaways')->document($takeawayId)->update([
                ['path' => 'cancelado', 'value' => false]
            ]);
            return response()->json(['msg'=>'Takeaway re-ativado'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function deactivate($takeawayId)
    {
        try{
            $this->firestore->collection('Takeaways')->document($takeawayId)->update([
                ['path' => 'cancelado', 'value' => true]
            ]);
            return response()->json(['msg'=>'Takeaway desativado'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function prepare($takeawayId)
    {
        try{
            $snapshot = $this->firestore->collection('Takeaways')->document($takeawayId)->snapshot();

            if ($snapshot->exists()) {
                if(!($snapshot->data()['estado'] === "aceite")){
                    return response()->json("Takeaway não se encontra no estado: aceite.", 400);
                }

                $this->firestore->collection('Takeaways')->document($takeawayId)->update([
                    ['path' => 'estado', 'value' => "em preparacao"]
                ]);

                if($snapshot->data()['utilizador'] !== null){
                    $this->sendNotification(
                        $snapshot->data()['utilizador'],
                        'Pedido em confeção', 
                        'Informamos que o seu pedido encontra-se em confeção.'
                    );
                }

                return response()->json(['msg'=>'Takeaway em confeção'], 200);
            }else{
                return response()->json("Invalid Takeaway id", 400);
            }
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function finish($takeawayId)
    {
        try{
            $snapshot = $this->firestore->collection('Takeaways')->document($takeawayId)->snapshot();

            if ($snapshot->exists()) {
                if(!($snapshot->data()['estado'] === "em preparacao")){
                    return response()->json("Takeaway não se encontra no estado: em preparação.", 400);
                }

                $this->firestore->collection('Takeaways')->document($takeawayId)->update([
                    ['path' => 'estado', 'value' => "pronto"]
                ]);
            
                if($snapshot->data()['utilizador'] !== null){
                    $this->sendNotification(
                        $snapshot->data()['utilizador'],
                        'Pedido pronto para levantar', 
                        'Informamos que o seu pedido está pronto para ser levantado.'
                    );
                }

                return response()->json(['msg'=>'Takeaway pronto para levantar'], 200);
            }else{
                return response()->json("Invalid Takeaway id", 400);
            }
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function lift($takeawayId)
    {
        try{
            $snapshot = $this->firestore->collection('Takeaways')->document($takeawayId)->snapshot();

            if ($snapshot->exists()) {
                if(!($snapshot->data()['estado'] === "pronto")){
                    return response()->json("Takeaway não se encontra no estado: pronto.", 400);
                }

                $this->firestore->collection('Takeaways')->document($takeawayId)->update([
                    ['path' => 'estado', 'value' => "levantado"]
                ]);
            
                if($snapshot->data()['utilizador'] !== null){
                    $this->sendNotification(
                        $snapshot->data()['utilizador'],
                        'Pedido levantado', 
                        'Obrigado e volte sempre.'
                    );
                }

                return response()->json(['msg'=>'Takeaway levantado'], 200);
            }else{
                return response()->json("Invalid Takeaway id", 400);
            }
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    private function sendNotification($topic, $title, $body){
        $config = AndroidConfig::fromArray([
            'ttl' => '3600s',
            'priority' => 'normal',
            'notification' => [
                'title' => $title,
                'body' => $body,
            ],
        ]);

        $message = CloudMessage::withTarget('topic', $topic)
            ->withAndroidConfig($config);
        $this->messaging->send($message);
    }
}
