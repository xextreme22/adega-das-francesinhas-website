<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use App\Http\Requests\PromotionRequest;
use App\Http\Requests\PromotionUpdateRequest;

class PromotionController extends Controller
{
    private $firebase;
    private $firestore;
    private $storageBucket;

    public function __construct(){
        $this->firebase = (new Factory())->withServiceAccount(base_path().'\\FirebaseKey.json');
        $this->firestore = $this->firebase->createFirestore()->database();
        $this->storageBucket = $this->firebase->createStorage()->getBucket();
    }

    public function index(Request $request)
    {
        $documents = $this->firestore->collection('Promocoes')->documents();

        $doc=null;
        $i=0;
        foreach ($documents as $document) {
            if($document->exists()){
                $doc[$i] = $document->data();
                $doc[$i]['id'] = $document->id();
                $i++;
            }
        }

        return response()->json(['promotions'=>$doc], 200);
    }

    public function store(PromotionRequest $request)
    {
        try{
            /*
            $nomeRestaurante = strtolower($request->nomeRestaurante);
            $nomeCategoria = strtolower($request->nomeCategoria);
            $nomeItem = strtolower($request->nomeItem);
            */
            $data = [
                'pontos' => intval($request->pontos),
                'restaurante' => $request->nomeRestaurante,
                'categoria' => $request->nomeCategoria,
                'item' => $request->nomeItem,
                'disabled' => false,
            ];
            $storedData = $this->firestore->collection('Promocoes')->add($data);
            $data['id'] = $storedData->id();
            return response()->json(['data'=>$data], 201);
        } catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function update(PromotionUpdateRequest $request, $promotionId)
    {
        $propertiesPromotion = [
            ['path' => 'pontos', 'value' => intval($request->pontos)]
        ];

        $this->firestore->collection('Promocoes')->document($promotionId)->update($propertiesPromotion);
        $promotionUpdated = [
            'pontos' => intval($request->pontos),
        ];

        return response()->json(['promotion'=>$promotionUpdated], 200);
    }

    public function activate($promotionId)
    {
        try{
            $this->firestore->collection('Promocoes')->document($promotionId)->update([
                ['path' => 'disabled', 'value' => false]
            ]);
            return response()->json(['msg'=>'Promoção ativada'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function deactivate($promotionId)
    {
        try{
            $this->firestore->collection('Promocoes')->document($promotionId)->update([
                ['path' => 'disabled', 'value' => true]
            ]);
            return response()->json(['msg'=>'Promoção desativada'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }
}
