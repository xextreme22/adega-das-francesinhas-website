<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use DateTimeZone;
use DateTime;
use DateInterval;
use Carbon\Carbon;

class DashboardController extends Controller
{
    private $firebase;
    private $firestore;
    private $storageBucket;

    public function __construct(){
        $this->firebase = (new Factory())->withServiceAccount(base_path().'\\FirebaseKey.json');
        $this->firestore = $this->firebase->createFirestore()->database();
        $this->storageBucket = $this->firebase->createStorage()->getBucket();
    }

    public function index(Request $request)
    {
        $today = new DateTime(Carbon::now());
        $today->sub(new DateInterval('PT1H'));
        $oneWeek = new DateTime(Carbon::now());
        $oneWeek->add(new DateInterval('P8D'));

        //get takeaways
        $documents = $this->firestore->collection('Takeaways')
                    ->where('pago', '=', true)
                    ->where('cancelado', '=', false)
                    ->where('data', '>', $today)
                    ->where('data', '<', $oneWeek)
                    ->documents();
        $docTakeaways=null;
        $i=0;
        foreach ($documents as $document) {
            if($document->exists()){
                if($document->data()['estado'] !== "levantado"){
                    $docTakeaways[$i] = $document->data();
                    $docTakeaways[$i]['id'] = $document->id();
                    $docTakeaways[$i]['data'] = $document->data()['data']->get()->setTimezone(new DateTimeZone('Europe/Lisbon'))->format('Y-m-d H:i');
                    $i++;
                }
            }
        }

        //get reservations
        $documents = $this->firestore->collection('Reservas')
                    ->where('cancelado', '=', false)
                    ->where('data', '>', $today)
                    ->where('data', '<', $oneWeek)
                    ->documents();
        $docReservations=null;
        $i=0;
        foreach ($documents as $document) {
            if($document->exists()){
                $docReservations[$i] = $document->data();
                $docReservations[$i]['id'] = $document->id();
                $docReservations[$i]['data'] = $document->data()['data']->get()->setTimezone(new DateTimeZone('Europe/Lisbon'))->format('Y-m-d H:i');
                $i++;
            }
        }

        return response()->json(['reservations'=>$docReservations, 'takeaways'=>$docTakeaways], 200);
    }
}
