<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use App\Http\Requests\MessageRequest;
use Illuminate\Support\Str;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\AndroidConfig;
use Google\Cloud\Core\Timestamp;
use \DateTime;
use Illuminate\Support\Facades\File;

class MessageController extends Controller
{
    private $firebase;
    private $firestore;
    private $storageBucket;
    private $messaging;

    public function __construct(){
        $this->firebase = (new Factory())->withServiceAccount(base_path().'\\FirebaseKey.json');
        $this->firestore = $this->firebase->createFirestore()->database();
        $this->storageBucket = $this->firebase->createStorage()->getBucket();
        $this->messaging = $this->firebase->createMessaging();
    }

    private function createDirectoryIfDoesntExist(){
        $path = 'storage\\fotos\\notificacoes';
        $pathDirectory = public_path().'\\' . $path;
        File::makeDirectory($path, 0777, true, true);
    }

    public function index(Request $request)
    {
        $this->createDirectoryIfDoesntExist();

        $documents = $this->firestore->collection('Notificacoes')->documents();

        $doc=null;
        $i=0;
        foreach ($documents as $document) {
            if($document->exists()){
                $doc[$i] = $document->data();
                $doc[$i]['id'] = $document->id();
                $image = 'storage\\fotos\\notificacoes\\'.$document->id().'.png';
                $doc[$i]['image'] = '\\'.$image;
                $doc[$i]['data'] = $document->data()['data']->get()->format('Y-m-d H:i');

                if (!file_exists( public_path() . '\\'.$image)) {
                    $object = $this->storageBucket->object('notificacoes/'.$document->id().'.png');
                    if($object->exists()){
                        $object->downloadToFile($image);
                    }else{
                        $doc[$i]['image'] = '\\static\\placeholder.png';
                    }
                }
                $i++;
            }
        }

        return response()->json(['notificacoes'=>$doc], 200);
    }

    public function store(MessageRequest $request)
    {
        try{
            if($request->image !== null){
                if (strpos($request->input('image'), 'data:image/') !== false) {
                    $exploded = explode(',', $request->image);
                    $image = base64_decode($exploded[1]);
                    if (!Str::contains($exploded[0], 'png')) {
                        return response()->json(['errors'=>array("image" => "Error incorrect Image extension (only accepts png)")], 422);
                    }
                    if(strlen($image) > 200000){
                        return response()->json(['errors'=>array("image" => "Image size cannot exceed 200KB for perfomance reasons on mobile phones")], 422);
                    }
                }else{
                    return response()->json(['errors'=>array("image" => "Image does not corresponde to an image file")], 422);
                }
            }else{
                return response()->json(['errors'=>array("image" => "Imagem é obrigatória")], 422);
            }

            //store notification
            date_default_timezone_set("Europe/Lisbon");
            $date = date('d/m/Y h:i:s a');
            $date = DateTime::createFromFormat('d/m/Y h:i:s a', $date);
            $timestamp = new Timestamp($date);

            $data = [
                'titulo' => $request->title,
                'mensagem' => $request->message,
                'data' => $timestamp,
                'disabled' => false
            ];
            $storedData = $this->firestore->collection('Notificacoes')->add($data);
            $id = $storedData->id();
            $data['id'] = $id;

            //store image
            $name_image='notificacoes/'.$id.".png";
            $this->storageBucket->upload(
                $image, 
                ['name' => $name_image]
            );
            $object = $this->storageBucket->object('notificacoes/'.$id.'.png');
            $path = 'storage/fotos/notificacoes/'.$id.'.png';
            $data['image'] = $path;
            $this->createDirectoryIfDoesntExist();
            $object->downloadToFile($path);


            //send notification
            $config = AndroidConfig::fromArray([
                'ttl' => '3600s',
                'priority' => 'normal',
                'notification' => [
                    'title' => $data['titulo'],
                    'body' => $data['mensagem'],
                ],
            ]);
            $topic = 'all';
            $message = CloudMessage::withTarget('topic', $topic)
                ->withAndroidConfig($config);
            $this->messaging->send($message);

            $data['data'] = $data['data']->get()->format('Y-m-d\TH:i');
            return response()->json(['data'=>$data], 201);
        } catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function activate($notificationId)
    {
        try{
            /*
            $id = strtolower($id);
            */
            $this->firestore->collection('Notificacoes')->document($notificationId)->update([
                ['path' => 'disabled', 'value' => false]
            ]);
            return response()->json(['msg'=>'Notificação ativada'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }

    public function deactivate($notificationId)
    {
        try{
            /*
            $id = strtolower($id);
            */
            $this->firestore->collection('Notificacoes')->document($notificationId)->update([
                ['path' => 'disabled', 'value' => true]
            ]);
            return response()->json(['msg'=>'Notificação desativada'], 200);
        }catch(Exception $e){
            return response()->json("Unexpected Error", 500);
        }
    }
}
