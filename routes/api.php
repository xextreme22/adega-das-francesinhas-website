<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**************FOR AUTHENTICATED USERS ROUTES*****************/
Route::middleware(['authenticated'])->group(function () {

    Route::post('logout', 'AuthController@logout');

});

/**************FOR EMPLOYEE USERS ROUTES*****************/
Route::middleware(['employee'])->group(function () {

    /*************DASHBOARD******************/
    Route::get('dashboard', 'DashboardController@index');

    /*************TAKEAWAYS******************/
    Route::get('takeaways', 'TakeawayController@index');
    Route::post('takeaway', 'TakeawayController@store');
    Route::get('takeaway/{takeawayId}', 'TakeawayController@show');
    Route::put('takeaway/{takeawayId}', 'TakeawayController@update');
    Route::put('takeaway/{takeawayId}/prepare', 'TakeawayController@prepare');
    Route::put('takeaway/{takeawayId}/finish', 'TakeawayController@finish');
    Route::put('takeaway/{takeawayId}/lift', 'TakeawayController@lift');
    
    /**************RESERVATIONS*****************/
    Route::get('reservations', 'ReservationController@index');
    Route::post('reservation', 'ReservationController@store');
    Route::get('reservation/{reservationId}', 'ReservationController@show');
    Route::put('reservation/{reservationId}', 'ReservationController@update');
    Route::put('reservation/{reservationId}/accept', 'ReservationController@accept');
    Route::put('reservation/{reservationId}/refuse', 'ReservationController@refuse');
});

/**************FOR ADMIN USERS ROUTES*****************/
Route::middleware(['administrator'])->group(function () {

    /**************USERS*****************/
    Route::get('users', 'UserController@index');
    Route::post('user', 'UserController@store');
    Route::put('user/{userId}', 'UserController@update');
    Route::put('user/{userId}/activate', 'UserController@activate');
    Route::put('user/{userId}/deactivate', 'UserController@deactivate');

    /**************RESTAURANTS*****************/
    Route::get('restaurants', 'RestaurantController@index');
    Route::post('restaurant', 'RestaurantController@store');
    Route::put('restaurant/{restaurantId}', 'RestaurantController@update');
    Route::put('restaurant/{restaurantId}/activate', 'RestaurantController@activate');
    Route::put('restaurant/{restaurantId}/deactivate', 'RestaurantController@deactivate');
    Route::get('restaurant/{restaurantId}/schedules', 'RestaurantController@schedules');
    Route::get('restaurant/{restaurantId}/categories', 'RestaurantController@categories');
    Route::get('restaurant/{restaurantId}/category/{categoryId}/menus', 'RestaurantController@menus');

    /***************SCHEDULES****************/
    Route::post('restaurant/{restaurantId}/schedule', 'ScheduleController@store');
    Route::put('restaurant/{restaurantId}/schedule/{scheduleId}', 'ScheduleController@update');
    Route::put('restaurant/{restaurantId}/schedule/{scheduleId}/activate', 'ScheduleController@activate');
    Route::put('restaurant/{restaurantId}/schedule/{scheduleId}/deactivate', 'ScheduleController@deactivate');
    Route::post('restaurant/{restaurantId}/scheduleException', 'ScheduleController@storeException');
    Route::put('restaurant/{restaurantId}/scheduleException/{exceptionId}', 'ScheduleController@updateException');
    Route::put('restaurant/{restaurantId}/scheduleException/{exceptionId}/activate', 'ScheduleController@activateException');
    Route::put('restaurant/{restaurantId}/scheduleException/{exceptionId}/deactivate', 'ScheduleController@deactivateException');

    /**************CATEGORIES*****************/
    Route::post('restaurant/{restaurantId}/category', 'CategoryController@store');
    Route::put('restaurant/{restaurantId}/category/{categoryId}/activate', 'CategoryController@activate');
    Route::put('restaurant/{restaurantId}/category/{categoryId}/deactivate', 'CategoryController@deactivate');
    Route::get('restaurant/{restaurantId}/category/{categoryId}/menus', 'CategoryController@menus');
    Route::get('restaurant/{restaurantId}/category/{categoryId}/menus/simplified', 'CategoryController@menusSimplified');

    /**************MENUS*****************/
    Route::post('restaurant/{restaurantId}/category/{categoryId}/menu', 'MenusController@store');
    Route::put('restaurant/{restaurantId}/category/{categoryId}/menu/{menuId}', 'MenusController@update');
    Route::put('restaurant/{restaurantId}/category/{categoryId}/menu/{menuId}/activate', 'MenusController@activate');
    Route::put('restaurant/{restaurantId}/category/{categoryId}/menu/{menuId}/deactivate', 'MenusController@deactivate');
    Route::get('restaurant/{restaurantId}/category/{categoryId}/menu/{menuId}/allergens', 'MenusController@allergens');

    /***************ALLERGENS****************/
    Route::post('restaurant/{restaurantId}/category/{categoryId}/menu/{menuId}/allergen', 'AllergenController@store');
    Route::put('restaurant/{restaurantId}/category/{categoryId}/menu/{menuId}/allergen/{allergenId}', 'AllergenController@update');
    Route::put('restaurant/{restaurantId}/category/{categoryId}/menu/{menuId}/allergen/{allergenId}/activate', 'AllergenController@activate');
    Route::put('restaurant/{restaurantId}/category/{categoryId}/menu/{menuId}/allergen/{allergenId}/deactivate', 'AllergenController@deactivate');

    /**************PROMOTIONS*****************/
    Route::get('promotions', 'PromotionController@index');
    Route::post('promotion', 'PromotionController@store');
    Route::put('promotion/{promotionId}', 'PromotionController@update');
    Route::put('promotion/{promotionId}/activate', 'PromotionController@activate');
    Route::put('promotion/{promotionId}/deactivate', 'PromotionController@deactivate');

    /**************MESSAGES*****************/
    Route::get('notifications', 'MessageController@index');
    Route::post('notification', 'MessageController@store');
    Route::put('notification/{notificationId}/activate', 'MessageController@activate');
    Route::put('notification/{notificationId}/deactivate', 'MessageController@deactivate');
    
    /**************TAKEAWAYS*****************/
    Route::put('takeaway/{takeawayId}/activate', 'TakeawayController@activate');
    Route::put('takeaway/{takeawayId}/deactivate', 'TakeawayController@deactivate');

    /**************RESERVATIONS*****************/
    Route::put('reservation/{reservationId}/activate', 'ReservationController@activate');
    Route::put('reservation/{reservationId}/deactivate', 'ReservationController@deactivate');
});

/**************FOR UN-AUTHENTICATED USERS ROUTES*****************/
Route::post('signInWithEmail', 'AuthController@signInWithEmail')->name('signInWithEmail'); 
Route::post('resetPassword', 'AuthController@resetPassword')->name('resetPassword'); 
Route::put('signInWithRefreshToken', 'AuthController@signInWithRefreshToken')->name('signInWithRefreshToken'); 
Route::get('restaurants/names', 'RestaurantController@names');
Route::get('restaurants/namesAndImages', 'RestaurantController@namesAndImages');
Route::get('restaurant/{restaurantId}/details', 'RestaurantController@details');
Route::get('restaurant/{restaurantId}/menu', 'RestaurantController@menu');